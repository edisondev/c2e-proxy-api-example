# README #

Easy to understand client code for c2e-proxy SOAP WebService. 
Available in Java and PHP so far.

### What is this repository for? ###

* To help customers with development of theri own client applications

### How do I get set up? ###

For Java example:
* Decent JDK (1.7 is fine)
* $JAVA_HOME/bin directory included in $PATH variable

First please run **01_compile.sh** (on Windows 01_compile.bat)
That will generate appropiate WS classes and compile two simple clients.

Please run **02_versionInfo.sh** (on Windows 02_versionInfo.bat)
That will run client code for AdminService and will print version information.

Please run **03_inboxList.sh** (on Windows 03_inboxList.bat) with two arguments: username and password, e.g.:

```
#!shell

03_inboxList.sh 0123456789 MYPASSWORD
```

That will run client code to iterate through documents in the inbox.


Run: **04_uploadDocument.sh** (Windows: 04_uploadDocument.bat) with arguments: username password receiverGln aprf snrf fileType fileToSend

This will try to send the document to receiver's inbox. 

FileType is: BINARY, CHARACTER, or XML
File to send: absolute or relative path to the file to be sent


```
#!shell

04_uploadDocument.sh 0123456789 MYPASSWORD 9876543210 810 001 BINARY /tmp/invoice.xml
```


For PHP example:
* Code was tested with PHP 5.5.9
* Please examine sample code in php directory

### Who do I talk to? ###

* Edison - http://www.edison.pl/