import c2e.proxy.ws.FileType;
import c2e.proxy.ws.UploadDocumentContext;
import c2e.proxy.ws.UploadFileResult;
import pl.net.edison.c2e.EdiService;
import pl.net.edison.c2e.EdiService_Service;

import java.io.File;
import java.nio.file.Files;

/**
 * @author wzupa
 */
public class UploadDocument {

    public static void main(String[] args) throws Exception {
        EdiService service = new EdiService_Service().getPort(EdiService.class);

        UploadDocumentContext ctx = new UploadDocumentContext();
        ctx.setRecipient(args[2]);
        ctx.setAprf(args[3]);
        ctx.setSnrf(args[4]);
        ctx.setFileType(FileType.fromValue(args[5]));
        ctx.setContent(Files.readAllBytes(new File(args[6]).toPath()));

        UploadFileResult uploadFileResult = service.uploadDocument(args[0], args[1], ctx);
        System.out.println("Code: " + uploadFileResult.getStatusCode());
        System.out.println("Message: " + uploadFileResult.getStatusCode());
        System.out.println("Ref: " +uploadFileResult.getServiceRef());
    }
}
