import pl.net.edison.c2e.*;
import c2e.proxy.ws.*;

public class InboxList {
    public static void main(String[] args) throws Exception {
	EdiService service = new EdiService_Service().getPort(EdiService.class);
	MailboxListResult inbox = service.getInboxList(args[0], args[1]);
	System.out.println("Documents: " + inbox.getDocuments().size());
	for(InboxDocumentItem document : inbox.getDocuments()) {
	    System.out.println("serviceRef: " + document.getServiceRef());
	}
    }
}