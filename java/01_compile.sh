#!/bin/sh

rm -rf *.class pl/* c2e/*

wsimport -keep "https://ewa.edison.pl/c2e-proxy/ws/EdiService?wsdl"
wsimport -keep "https://ewa.edison.pl/c2e-proxy/ws/AdminService?wsdl"
#wsimport -keep "https://efaktura.edison.net.pl/c2e-proxy/ws/EdiService?wsdl"
#wsimport -keep "https://efaktura.edison.net.pl/c2e-proxy/ws/AdminService?wsdl"

javac -classpath . *.java
