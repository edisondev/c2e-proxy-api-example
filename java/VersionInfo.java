import pl.net.edison.c2e.*;
import c2e.proxy.ws.*;

public class VersionInfo {
    public static void main(String[] args) {
	AdminService service = new AdminService_Service().getPort(AdminService.class);
	VersionInformation version = service.getVersionInformation();

	System.out.println(version.getImplementationName());
	System.out.println(version.getImplementationVersion());
    }
}